package com.herwijnen;

public class Pepsi extends ColdDrink {

    private Pepsi(){}

    private static class Helper{
        private static final Pepsi pepsi = new Pepsi();
    }

    public static Pepsi getInstance(){
        return Helper.pepsi;
    }
    @Override
    public String getName() {
        return "Pepsi Cola";
    }

    @Override
    public double getPrice() {
        return 2.22;
    }
}
