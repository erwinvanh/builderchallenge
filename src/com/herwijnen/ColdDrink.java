package com.herwijnen;

public abstract class ColdDrink implements Item {
    @Override
    public abstract String getName();

    @Override
    public abstract double getPrice();

    @Override
    public Packing getPacking() {
        return Bottle.getInstance();
    }
}
