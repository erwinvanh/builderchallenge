package com.herwijnen;

public class ChickenBurger extends Burger {

    private ChickenBurger(){}

    private static class Helper {
        private static final ChickenBurger chickenBurger = new ChickenBurger();
    }

    public static ChickenBurger getInstance(){
        return Helper.chickenBurger;
    }
    @Override
    public String getName() {
        return "ChickenBurger";
    }

    @Override
    public double getPrice() {
        return 3.99;
    }
}
