package com.herwijnen;

public class VegMealBuilder implements MealBuilder {
    private Meal meal = new Meal();

    @Override
    public void buildBurger() {
        meal.addItem(VegBurger.getInstance());
    }

    @Override
    public void buildDrink() {
        meal.addItem(Pepsi.getInstance());
    }

    @Override
    public Meal getMeal() {
        return meal;
    }
}
