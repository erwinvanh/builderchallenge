package com.herwijnen;

public class Wrapper implements Packing {
    private Wrapper(){}

    private static class Helper{
        private final static Wrapper packing = new Wrapper();
    }

    public static Wrapper getInstance(){
        return Helper.packing;
    }

    @Override
    public String pack() {
        return "Wrapper";
    }
}
