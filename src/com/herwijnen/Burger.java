package com.herwijnen;

public abstract class Burger implements Item{
//    @Override
//    public abstract String getName();

    @Override
    public abstract double getPrice();

    @Override
    public Packing getPacking() {
        return Wrapper.getInstance();
    }
}
