package com.herwijnen;

public class Bottle implements Packing {
    private Bottle(){}

    private static class Helper{
        private final static Bottle packing = new Bottle();
    }

    public static Bottle getInstance(){
        return Helper.packing;
    }

    @Override
    public String pack() {
        return "bottle";
    }
}
