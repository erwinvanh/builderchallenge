package com.herwijnen;

public class VegBurger extends Burger {

    private VegBurger(){}
    private static class Helper {
        private static final VegBurger vegBurger = new VegBurger();
    }

    public static VegBurger getInstance(){
        return Helper.vegBurger;
    }
    @Override
    public String getName() {
        return "Veggie Burger";
    }

    @Override
    public double getPrice() {
        return 5.75;
    }
}
