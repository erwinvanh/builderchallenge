package com.herwijnen;

public class Client {

    public static void main(String[] args) {
        System.out.println("*** Builder Pattern Challenge ***\n");
        Director director = new Director();

        MealBuilder veggieBuilder = new VegMealBuilder();
        director.construct(veggieBuilder);
        Meal veggie = veggieBuilder.getMeal();
        veggie.showItems();
        System.out.println("Total cost for the veggie meal are " + String.format("%.2f",veggie.getCost()));

        MealBuilder nonVegBuilder = new NonVegMealBuilder();
        director.construct(nonVegBuilder);
        Meal nonVeggie = nonVegBuilder.getMeal();
        nonVeggie.showItems();
        System.out.println("Total cost for the non-veggie meal are " + String.format("%.2f",nonVeggie.getCost()));
    }
}
