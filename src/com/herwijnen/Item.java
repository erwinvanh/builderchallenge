package com.herwijnen;

interface Item {
    String getName();
    double getPrice();
    Packing getPacking();
}
