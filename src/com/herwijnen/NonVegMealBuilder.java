package com.herwijnen;

public class NonVegMealBuilder implements MealBuilder {
    private Meal meal = new Meal();
    @Override
    public void buildBurger() {
        meal.addItem(ChickenBurger.getInstance());
    }

    @Override
    public void buildDrink() {
        meal.addItem(Coke.getInstance());
    }

    @Override
    public Meal getMeal() {
        return meal;

    }
}
