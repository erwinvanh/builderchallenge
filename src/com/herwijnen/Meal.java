package com.herwijnen;

import java.util.LinkedList;

public class Meal {
    private LinkedList<Item> items;

    public Meal(){
        items = new LinkedList<>();
    }

    public void addItem(Item item){
        items.add(item);
    }

    public double getCost(){
        double totalCost = 0.0;
        for (Item item : items) {
            totalCost += item.getPrice();
        }
        return totalCost;
    }

    public void showItems() {
        System.out.println("Meal consists of :");
        for (Item item : items) {
            System.out.println("Item : " + item.getName() + ", Packing : " + item.getPacking().pack() + ", price : " + item.getPrice());
        }
    }

}
