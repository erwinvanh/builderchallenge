package com.herwijnen;

public class Coke extends ColdDrink {

    private Coke(){}

    private static class Helper{
        private static final Coke coke = new Coke();
    }

    public static Coke getInstance(){
        return Helper.coke;
    }
    @Override
    public String getName() {
        return "Coca Cola";
    }

    @Override
    public double getPrice() {
        return 1.99;
    }
}
